# Accès aux données ouvertes de l'Union européenne:
url_europe_covid_data = 'https://opendata.ecdc.europa.eu/covid19/casedistribution/csv'
import pandas as pd
data_eu = pd.read_csv(url_europe_covid_data)
print(data_eu.keys())