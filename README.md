# covid19 #

### What is this repository for? ###

* get COVID deaths data to plot and analyze with logistic function fitting
* data sources: 
    * https://data.europa.eu/euodp/fr/data/dataset/covid-19-coronavirus-data
    * https://github.com/CSSEGISandData/COVID-19
* logistic function source:
    * https://fr.wikipedia.org/wiki/Fonction_logistique_(Verhulst)

### How do I get set up? ###

* python covid19.py get : to get cvs files
* python covid19.py plot : to plot data for a few countries with major breakout
* python covid19.py world : to plot world data by continents
* python covid19.py countries : for a list of countries in the database sorted by continents
