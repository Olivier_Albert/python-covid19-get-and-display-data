#### FROM: https://towardsdatascience.com/infectious-disease-modelling-fit-your-model-to-coronavirus-data-2568e672dbc7


import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import lmfit
from lmfit.lineshapes import gaussian, linear
from lmfit.models import LinearModel, GaussianModel, LognormalModel, PolynomialModel

import warnings
warnings.filterwarnings('ignore')



def lmfit_LogNormal_linear(x,data):
    # define a GaussianModel or LognormalModel for the peak 

    # peak = GaussianModel()
    peak = LognormalModel()
    peak.set_param_hint("height", value=np.max(data), vary=True) # auto detect maximum
    peak.set_param_hint("center", value=x[np.argmax(data)], vary=True) # auto detect maximum position
    print(np.max(data),x[np.argmax(data)])
    peak.set_param_hint("sigma", value=5.0, vary=True) #

    params = peak.make_params()

    model = peak 

    result = model.fit(data, params, method="leastsq", x=x)  # fitting

    plt.figure(figsize=(8,4))
    result.plot_fit(datafmt="-")
    plt.show()
    print(result.message)
    if result.message == 'Fit succeeded.':
        print(result.best_values)
        print(result.values)

        # reuse peak fit values for param hints and add LinearModel for background

        # peak = GaussianModel()
        peak = LognormalModel()
        peak.set_param_hint("amplitude", value=result.best_values['amplitude'], vary=True)
        peak.set_param_hint("center", value=result.best_values['center'], vary=True)
        peak.set_param_hint("sigma", value=result.best_values['sigma'], vary=True)

        background = LinearModel()
        background.set_param_hint('slope',value=0, vary=True)
        background.set_param_hint('intercept',value=0, vary=True)

        params = peak.make_params() + background.make_params()

        model = peak + background

        result = model.fit(data, params, method="leastsq", x=x)  # fitting

        plt.figure(figsize=(8,4))
        result.plot_fit(datafmt="-")
        print(result.message)
        print(result.best_values)
        # print(result.method)
        # print(result.covar)
        plt.grid()
        plt.show()

if __name__ == '__main__':
    # Curve Fitting Example
    # We want to fit the following curve:

    np.random.seed(42)
    x = np.linspace(0, 20.0, 1001)

    data = (gaussian(x, 21, 6.1, 1.2) +linear(x,slope=0.1,intercept=5.0)+ np.random.normal(scale=0.2, size=x.size))  # normal distr. with some noise
    lmfit_LogNormal_linear(x,data)