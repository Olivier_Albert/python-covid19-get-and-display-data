#!/usr/bin/env python
# -*- coding: utf-8 -*-

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def rolling_mean(x,N):
    rm = pd.Series(x).rolling(window=N).mean().iloc[N-1:].values # 
    Nn = int((N-1)/2)
    a1 = np.full(Nn, np.nan)
    a2 = np.full(N-Nn-1, np.nan)
    rm = np.concatenate((a1, rm, a2), axis=0)
    return rm
if __name__ == '__main__':

    max = 1
    ndp = 256
    t = np.arange(0,max,max/ndp)
    f= np.sin(t*10)+np.random.randn(len(t))*0.075
    plt.scatter(t,f)
    plt.plot(t,rolling_mean(f,10),'r')
    plt.show()

    # print(f" {len(rolling_mean(f,10))},\n {rolling_mean(f,10)}")