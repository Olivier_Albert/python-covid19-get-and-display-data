# Savitzky-Golay filter. 
# It uses least squares to regress a small window of your data onto a polynomial, 
# then uses the polynomial to estimate the point in the center of the window. 
# Finally the window is shifted forward by one data point and the process repeats. 
# This continues until every point has been optimally adjusted relative to its neighbors. 
# It works great even with noisy samples from non-periodic and non-linear sources. 
# the Savitzky-Golay filter has been incorporated into the SciPy library
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

x = np.linspace(0,2*np.pi,100)
y = np.sin(x) + np.random.random(100) * 0.2
yhat = savgol_filter(y, 51, 3) # window size 51, polynomial order 3

plt.plot(x,y)
plt.plot(x,yhat, color='red')
plt.show()