### get COVID deaths data to plot and analyze
# 
# https://data.europa.eu/euodp/fr/data/dataset/covid-19-coronavirus-data
# données régionales: 
# https://www.coronavirus-statistiques.com/corostats/openstats/open_stats_coronavirus.csv
#
# Accès aux données ouvertes de l'Union européenne:
url_europe_covid_data = 'https://opendata.ecdc.europa.eu/covid19/casedistribution/csv'
# local datafile H5
df_file = r"C:\Users\olivi\Documents\python\prog\covid19.h5"
# local graph file
save_file = r"C:\Users\olivi\Documents\python\prog\covid19.png"
save_world_file = r"C:\Users\olivi\Documents\python\prog\covid19_world.png"
# imports
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import dates
import numpy as np
from datetime import datetime, timedelta
import pycountry_convert as pc
import locale
locale.setlocale(locale.LC_ALL, '')
from Covid19.rollingMean import rolling_mean

# functions
def store_data():
    # european Covid19 data
    try:
        data_eu = pd.read_csv(url_europe_covid_data)
        data_eu = data_eu.astype({'dateRep':str, 'countriesAndTerritories':str, 'geoId':str, 'countryterritoryCode':str})
        europe = True
        print('data from données ouvertes de l Union européenne downloaded.')
    except:
        europe = False
        print('data from données ouvertes de l Union européenne unavailable.')
    # open H5 file
    store = pd.HDFStore(df_file)
    if europe : store['europe'] = data_eu
    store.close()
    return None

def load_data(type = 'europe'): 
    store = pd.HDFStore(df_file)
    df = store[type]  # load it
    store.close()
    return df

def continents_from_geoId(geoid = "FR"):
    # get continent from country code
    continent_code = None
    continent_name = None
    if geoid == "UK": 
        continent_code = pc.country_alpha2_to_continent_code('GB') # get continent code for UK as GB
        continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
    elif geoid == "EL":
        continent_code = pc.country_alpha2_to_continent_code('GR') # get continent code for Greece (EL is in europe only...)
        continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
    elif geoid == "VA": # Vatican
        continent_code = 'EU'
        continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
    elif geoid == "SX": # Saint Martin
        continent_code = 'NA'
        continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
    elif geoid == "TL": # East Timor
        continent_code = 'OC'
        continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
    else:
        try:
            continent_code = pc.country_alpha2_to_continent_code(geoid) # get continent code
            continent_name = pc.convert_continent_code_to_continent_name(continent_code) # get continent name
        except:
            continent_code = 'inconnu'
            continent_name = 'inconnu'
    return continent_code, continent_name

def list_countries(df):
    if 'countriesAndTerritories' in df.columns: # to make sure this field exist 
        country_list = []
        geoid_list = list(df['geoId'])
        geoid_list = sorted(list(set(geoid_list))) # remove duplicates and sort
        for geoid in geoid_list:
            continent_code, continent_name = continents_from_geoId(geoid)
            # cont_name = df[df['geoId'] == geoid]
            # first_row = cont_name.iloc[0]
            # continent_name = first_row['continentExp']
            country_list.append([geoid,continent_name,continent_code])
        # sort countries by continent
        # get continents
        list_c = country_list.copy()
        continents = []
        for item in list_c:
            if item[1] not in continents: 
                continents.append(item[1])
        # sort countries by continents
        world = []
        for continent in continents:
            temp_list = []
            for item in list_c:
                if item[0] == 'nan' or len(item[0]) > 2 :
                    pass
                else: 
                    if item[1] is continent : temp_list.append(item[0])
            if continent != 'inconnu' and len(temp_list)>0 : world.append([continent,temp_list])
        return country_list, world
    else: 
        return None

def data_for_country_euro_db(df, country = 'France'):
    ### As UE database for covid19 is better formated, 
    # and provide more info like Country Id and population, 
    # Input: df and country name. 
    # Output: A dict containing: 
    #  - dates list in datetime format.
    #  - cases and cases_rate in array format.
    #  - deaths and death_rate in array format.
    #  - population as of 2018 numbers.
    #  - geoID.
    #  - ContryCode : Country code.
    if len(country) == 2 : 
        country_mask = df['geoId'] == country # define df country mask with geoId
    else:
        country_mask = df['countriesAndTerritories'] == country # define df country mask with country name
    df_country = df[country_mask]  # get df for country
    dates = list(df_country['dateRep']) # get list of date strings
    dates_list = [datetime.strptime(x,'%d/%m/%Y') for x in dates] # convert date strings do datetime
    dates_list =list( reversed(dates_list))
    death_rate = np.array(list(reversed(list(df_country['deaths_weekly']))))
    deaths = np.cumsum(death_rate) # cumulated deaths data
    cases_rate = np.array(list(reversed(list(df_country['cases_weekly']))))
    cases = np.cumsum(cases_rate) # cumulated cases data
    first_row = df_country.iloc[0]
    if first_row['popData2019'] > 0 : 
        population = first_row['popData2019']
    else:
        population = 1
    # get continent infos
    continent_code, continent_name = continents_from_geoId(first_row['geoId'])
    # continent_name = first_row['continentExp']
    # # print(continent_name)
    # building dict
    country_dict ={
        'dates' : dates_list,
        'cases' : cases,
        'cases_rate' : cases_rate,
        'deaths' : deaths,
        'death_rate' : death_rate,
        'population' : population,
        'name' : first_row['countriesAndTerritories'],
        'geoId' : first_row['geoId'], 
        'countryCode' : first_row['countryterritoryCode'],
        'continent_code' : continent_code,
        'continent' : continent_name,
    }
    return country_dict

def plot_countries_uedb(countries_displayed, fl = False):
    df = load_data('europe')
    ## matplotlib date format object
    hfmt = dates.DateFormatter('%m/%y')
    # figure definition
    fig = plt.figure()
    fig.canvas.set_window_title('Covid19 data')
    ax = fig.add_subplot(241)   # Deaths
    axd = fig.add_subplot(245)  # Deaths per day
    axp = fig.add_subplot(242)   # Deaths /100 000
    axdp = fig.add_subplot(246)  # Deaths per day /100 000
    axc = fig.add_subplot(243)  # Confirmed cases
    axcd = fig.add_subplot(247) # Confirmed cases per day
    axcp = fig.add_subplot(244)  # Confirmed cases / 100 000
    axcdp = fig.add_subplot(248) # Confirmed cases per day /100 000

    for Country in countries_displayed:   
        # get data for country
        country_dict = data_for_country_euro_db(df,Country)
        dates_list = country_dict['dates'].copy()
        # get population: 
        pop = country_dict['population']
    
        ### plot data
        ## death
        ax.plot(country_dict['dates'],country_dict['deaths'],label=f"{country_dict['name']}: {country_dict['deaths'][-1]:n}")
        ## death rate
        # remove negative values
        data = country_dict['death_rate']
        data[data < 0] = 0
        axd.plot(country_dict['dates'],data,label=f"{country_dict['name']} ({country_dict['death_rate'][-1:][0]:.0f} this week)") 

        ## death /100 000
        axp.plot(country_dict['dates'],country_dict['deaths']/pop*100000,label=f"{country_dict['name']}: {country_dict['deaths'][-1]/pop*100000:.1f}")
        ## death rate /100 000
        # remove negative values
        data = country_dict['death_rate']
        data[data < 0] = 0
        axdp.plot(country_dict['dates'],data/pop*100000, label=f"{country_dict['name']} (~{np.mean(country_dict['death_rate'][-7:]/pop*100000):.1f} this week)") 

        ## cases
        axc.plot(country_dict['dates'],country_dict['cases'],label='{}: {:n}'.format(country_dict['name'],country_dict['cases'][-1]))
        ## cases rate
        # remove negative values
        data = country_dict['cases_rate']
        data[data < 0] = 0
        axcd.plot(country_dict['dates'],country_dict['cases_rate'],label=f"{country_dict['name']} {country_dict['cases_rate'][-1:][0]:.0f} this week)") 
    
        ## cases /100 000
        axcp.plot(country_dict['dates'],country_dict['cases']/pop*100000, label=f"{country_dict['name']}: {country_dict['cases'][-1]/pop*100000:.0f}") 
        ## cases rate /100 000
        # remove negative values
        data = country_dict['cases_rate']
        data[data < 0] = 0
        axcdp.plot(country_dict['dates'],data/pop*100000, label=f"{country_dict['name']} (~{np.mean(country_dict['cases_rate'][-7:]/pop*100000):.1f} this week)") #,label = Country)
    ### graph parameters
    legend_size = 8
    # graph fit logistique morts
    ax.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1)) # ax.xaxis.set_major_locator(dates.WeekdayLocator(6))
    ax.xaxis.set_major_formatter(hfmt)
    ax.tick_params(labelrotation=45)
    ax.legend(loc='best', prop={'size': legend_size})
    ax.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    ax.grid()
    ax.set_title('Deaths')
    # ax.set_ylabel("Deaths")
    # graph variation journalière de la mortalité
    axd.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axd.xaxis.set_major_formatter(hfmt)
    axd.tick_params(labelrotation=45)
    axd.legend(loc='best', prop={'size': legend_size})
    axd.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axd.grid()
    axd.set_title('Deaths / week')
    # axd.set_ylabel("Deaths / week")
    # graph  deaths /100 000
    axp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axp.xaxis.set_major_formatter(hfmt)
    axp.tick_params(labelrotation=45)
    axp.legend(loc='best', prop={'size': legend_size})
    axp.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axp.grid()
    axp.set_title('deaths / 100 000')
    # axp.set_ylabel("death / 100 000")
    # graph death / week /100 000
    axdp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axdp.xaxis.set_major_formatter(hfmt)
    axdp.tick_params(labelrotation=45)
    axdp.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axdp.legend(loc='best', prop={'size': legend_size})
    axdp.grid()
    axdp.set_title('Deaths / week / 100 000')
    # axcdp.set_xlabel("dates")
    # axcdp.set_ylabel("Deaths/ week / 100 000")
    # graph cases
    axc.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axc.xaxis.set_major_formatter(hfmt)
    axc.tick_params(labelrotation=45)
    axc.legend(loc='best', prop={'size': legend_size})
    axc.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axc.grid()
    axc.set_title('Cases')
    # axc.set_ylabel("Cases")
    # graph variation journalière des cas confirmés
    axcd.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1)) 
    axcd.xaxis.set_major_formatter(hfmt)
    axcd.tick_params(labelrotation=45)
    axcd.legend(loc='best', prop={'size': legend_size})
    axcd.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axcd.grid()
    axcd.set_title('Cases / week')
    # axcd.set_xlabel("dates")
    # axcd.set_ylabel("Cases / week")
    # graph  cases /100 000
    axcp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axcp.xaxis.set_major_formatter(hfmt)
    axcp.tick_params(labelrotation=45)
    axcp.legend(loc='best', prop={'size': legend_size})
    axcp.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axcp.grid()
    axcp.set_title('cases / 100 000')
    # axcp.set_ylabel("cases / 100 000")
    # graph variation journalière des cas /100 000
    axcdp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axcdp.xaxis.set_major_formatter(hfmt)
    axcdp.tick_params(labelrotation=45)
    axcdp.set_xlim(country_dict['dates'][0],country_dict['dates'][-1])
    axcdp.legend(loc='best', prop={'size': legend_size})
    axcdp.grid()
    axcdp.set_title('Cases / week / 100 000')
    # axcdp.set_xlabel("dates")
    # axcdp.set_ylabel("cases/ week / 100 000")
    plt.show()
    return fig

def world_plot():
    df = load_data('europe')
    list_c, world = list_countries(df)
    ## matplotlib date format object
    hfmt = dates.DateFormatter('%m/%y')
    # figure definition
    fig = plt.figure()
    fig.canvas.set_window_title('Covid19 World data')
    ax = fig.add_subplot(241)   # Deaths
    axd = fig.add_subplot(245)  # Deaths per day
    axp = fig.add_subplot(242)   # Deaths /100 000
    axdp = fig.add_subplot(246)  # Deaths per day /100 000
    axc = fig.add_subplot(243)  # Confirmed cases
    axcd = fig.add_subplot(247) # Confirmed cases per day
    axcp = fig.add_subplot(244)  # Confirmed cases / 100 000
    axcdp = fig.add_subplot(248) # Confirmed cases per day /100 000
    #
    deaths_per_continent = []
    cases_per_continent = []
    length = 0
    dates_list = []
    for item in world:
        print(item[0],'\n',item[1])
        table = []
        table_c = []    
        population = 0
        # get data length and dates list        
        for ct in item[1]:
            data = data_for_country_euro_db(df, country = ct)
            table.append(data['death_rate'])
            table_c.append(data['cases_rate'])
            if len(data['death_rate']) > length : 
                length = len(data['death_rate'])
                dates_list = data['dates']
            population += data['population']
        print(f"population: {population:n}")
        # build death rate data
        square_table = []
        for line in table:
            if len(line) == length : 
                square_table.append(line)
            else:
                line = np.pad(line, (length-len(line), 0), 'constant', constant_values=(0, 0)) # zero padding for country with late entry in the Covid discovery
                square_table.append(line)
        square_table = np.asarray(square_table)
        deaths_per_continent.append([item[0],np.sum(square_table, axis = 0),population]) # sum data from all country of a continent
        # build cases rate data
        square_table = []
        for line in table_c:
            if len(line) == length : 
                square_table.append(line)
            else:
                line = np.pad(line, (length-len(line), 0), 'constant', constant_values=(0, 0)) # zero padding for country with late entry in the Covid discovery
                square_table.append(line)
        square_table = np.asarray(square_table)
        cases_per_continent.append([item[0],np.sum(square_table, axis = 0),population]) # sum data from all country of a continent
    # plot deaths and death rate
    for cont in deaths_per_continent:
        # death rate
        axd.plot(dates_list,cont[1], label = cont[0])
        # deaths
        ax.plot(dates_list,np.cumsum(cont[1]), label = f"{cont[0]}: {np.max(np.cumsum(cont[1])):n}")
        # death rate /100 000
        axdp.plot(dates_list,cont[1]/cont[2]*100000, label = cont[0])
        # deaths /100 000
        axp.plot(dates_list,np.cumsum(cont[1]/cont[2]*100000), label = f"{cont[0]}: {np.max(np.cumsum(cont[1]/cont[2]*100000)):.1f}")
    # plot cases and cases rate
    for cont in cases_per_continent:
        # cases rate
        axcd.plot(dates_list,cont[1], label = cont[0])
        # cases 
        axc.plot(dates_list,np.cumsum(cont[1]), label = f"{cont[0]}: {np.max(np.cumsum(cont[1])):n}")
        # cases rate /100 000
        axcdp.plot(dates_list,cont[1]/cont[2]*100000, label = cont[0])
        # cases /100 000
        axcp.plot(dates_list,np.cumsum(cont[1]/cont[2]*100000), label = f"{cont[0]}: {np.max(np.cumsum(cont[1]/cont[2]*100000)):.0f}")
    # graph morts
    ax.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    ax.xaxis.set_major_formatter(hfmt)
    ax.tick_params(labelrotation=45)
    ax.set_xlim(dates_list[0],dates_list[-1]) #autoscale(enable=True, axis='x', tight=True)
    ax.legend()
    ax.grid()
    ax.set_title('Mortalité total')
    ax.set_ylabel("nombre de morts déclarés")
    # graph variation journalière de la mortalité
    axd.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axd.xaxis.set_major_formatter(hfmt)
    axd.tick_params(labelrotation=45)
    axd.set_xlim(dates_list[0],dates_list[-1])
    axd.legend()
    axd.grid()
    axd.set_title('Variation journalière de la mortalité')
    axd.set_ylabel("nombre de morts déclarés par jour")
    # graph morts /100 000
    axp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axp.xaxis.set_major_formatter(hfmt)
    axp.tick_params(labelrotation=45)
    axp.set_xlim(dates_list[0],dates_list[-1]) #autoscale(enable=True, axis='x', tight=True)
    axp.legend()
    axp.grid()
    axp.set_title('Mortalité total /100 000')
    axp.set_ylabel("nombre de morts/100 000 déclarés")
    # graph variation journalière de la mortalité /100 000
    axdp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axdp.xaxis.set_major_formatter(hfmt)
    axdp.tick_params(labelrotation=45)
    axdp.set_xlim(dates_list[0],dates_list[-1])
    axdp.legend()
    axdp.grid()
    axdp.set_title('Variation journalière de la mortalité/100 000')
    axdp.set_ylabel("nombre de morts/100 000 déclarés par jour")
    # graph confirmed cases
    axc.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axc.xaxis.set_major_formatter(hfmt)
    axc.tick_params(labelrotation=45)
    axc.set_xlim(dates_list[0],dates_list[-1])
    axc.legend()
    axc.grid()
    axc.set_title('Cas confirmés')
    axc.set_ylabel("Nombre de cas confirmés")
    # graph variation journalière des cas confirmés
    axcd.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axcd.xaxis.set_major_formatter(hfmt)
    axcd.tick_params(labelrotation=45)
    axcd.set_xlim(dates_list[0],dates_list[-1])
    axcd.legend()
    axcd.grid()
    axcd.set_title('Variation journalière des cas confirmés')
    axcd.set_ylabel("nombre de cas confirmés par jour")
    # graph confirmed cases /100 000
    axcp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axcp.xaxis.set_major_formatter(hfmt)
    axcp.tick_params(labelrotation=45)
    axcp.set_xlim(dates_list[0],dates_list[-1])
    axcp.legend()
    axcp.grid()
    axcp.set_title('Cas confirmés /100 000')
    axcp.set_ylabel("Nombre de cas confirmés /100 000")
    # graph variation journalière des cas confirmés /100 000
    axcdp.xaxis.set_major_locator(dates.MonthLocator(bymonthday=1))
    axcdp.xaxis.set_major_formatter(hfmt)
    axcdp.tick_params(labelrotation=45)
    axcdp.set_xlim(dates_list[0],dates_list[-1])
    axcdp.legend()
    axcdp.grid()
    axcdp.set_title('Variation journalière des cas confirmés /100 000')
    axcdp.set_ylabel("nombre de cas confirmés par jour /100 000")
    # dislay figure
    plt.show()
    return fig


if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        print('Usage: \n python covid19.py get # load data from sources \n python covid19.py plot # display Covid data graphs and save it to png \n python covid19.py world #display data by continent')
    if "get" in sys.argv : 
        store_data()
        df = load_data('europe')
        france = data_for_country_euro_db(df, country = 'France')   
        print(france['dates'][-1].strftime('updated up to : %d/%m/%Y'))
    if "countries" in sys.argv : 
        tmp, ctr = list_countries(load_data('europe'))
        for item in ctr:
            print(item[0],'\n',item[1])
    if "plot" in sys.argv:
        countries_displayed = ["FR","IT","ES","UK","US","BR","RU","IN","CN","ZA"] # using geoId
        fig = plot_countries_uedb(countries_displayed, fl =True)
        fig.savefig(save_file, dpi = 300)
    if "france" in sys.argv:
        countries_displayed = ["FR"] # using geoId "FR","IT","ES",
        fig = plot_countries_uedb(countries_displayed, fl =True)
    if "voisins" in sys.argv:
        countries_displayed = ["FR","IT","ES","UK","DE","BE","CH","SE"] # using geoId "FR","IT","ES",
        fig = plot_countries_uedb(countries_displayed, fl =True)
    if "world" in sys.argv:
        fig = world_plot()
        fig.savefig(save_world_file, dpi = 300)
    if "test" in sys.argv :
        df = load_data('europe')
        print(df.columns)        


#%%
# print('hello')

# %%
